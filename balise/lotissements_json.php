<?php

if (!defined("_ECRIRE_INC_VERSION")) return;    #securite

function balise_LOTISSEMENTS_JSON($p) { 
    return calculer_balise_dynamique($p,'LOTISSEMENTS_JSON', array());
}

function balise_LOTISSEMENTS_JSON_stat($args, $filtres) {
    return array($args[0]);
}   

function balise_LOTISSEMENTS_JSON_dyn($args, $filtres) {
    $lotissements = array();
    $json = array();
    $email = '';
    $i = 0;
    if ($_GET.array_key_exists('email'))
	$email = $_GET['email'];
    if ($email) {
    $result = spip_query("SELECT lotis_rubriques.titre
		    FROM lotis_zones_liens, lotis_rubriques
		    WHERE lotis_zones_liens.id_objet = lotis_rubriques.id_rubrique
		    AND lotis_zones_liens.objet = 'rubrique'
		    AND lotis_rubriques.id_parent = 1
		    AND lotis_zones_liens.id_zone IN (
			SELECT lotis_zones_liens.id_zone
                        FROM  lotis_zones_liens, lotis_auteurs
                        WHERE lotis_zones_liens.objet = 'auteur'
                        AND lotis_auteurs.email = '$email'
                        AND lotis_zones_liens.id_objet = lotis_auteurs.id_auteur
			)
		    ");
    } else {
	    $result = spip_query("SELECT lotis_rubriques.titre FROM lotis_rubriques WHERE lotis_rubriques.id_parent = 1");
    }
    while ($row = spip_fetch_array($result)) {
    	array_push($lotissements, array('id' => $i, 'text' => $row['titre']));
        $i += 1;
    }
    $json['data'] = $lotissements;
    return json_encode($json);
}

?>
